#pragma once
#define STACK_SIZE 500
#include "Position.h"
class Stack
{
public:
	Stack();
	~Stack();
	void push(Position x);
	Position pop();
	int stackCount;
	Position * pstnList;
	bool isEmpty();
private:
	
	
	bool isFull();
	
};