#pragma once
#include <string>
#include <stdio.h>
#include "Position.h"
using namespace std;
class Maze
{
public:
	Maze(char * infile);
	~Maze();
	char tile[50][50];
	int width;
	int height;
	Position Start;
	Position Goal;
private:
	char * file;
};