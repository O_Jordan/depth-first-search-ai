#pragma once
#include "Position.h"
class Node
{
public:
	Node *link[4];
	int linkCount;
	char key;
	Position pos;
	bool visited;
	int distance;
	Node();
	void AddLink(Node *toLink);
};