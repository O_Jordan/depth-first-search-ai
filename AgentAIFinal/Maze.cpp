#include "Maze.h"
#include <iostream>
Maze::Maze(char * infile)
{
	char buffer[50];
	file = infile;
	FILE *mazeFile;
	mazeFile = fopen(file,"r");
	fscanf(mazeFile, "%s", buffer);
	while (!feof(mazeFile))
	{
		if (strcmp(buffer, "#width") == 0)
		{
			char tmp[10];
			fscanf(mazeFile, "%s", tmp);
			width = atoi(tmp);
		}
		else if (strcmp(buffer, "#height") == 0)
		{
			char tmp[10];
			fscanf(mazeFile, "%s", tmp);
			height = atoi(tmp);
		}
		else if (strcmp(buffer, "#begin") == 0)
		{
			if (width <= 50 && height <= 50)
			{
				fscanf(mazeFile, "%s", buffer);
				for (int i = 0; i < height; i++)
				{
					for (int j = 0; j < width; j++)
					{
						tile[i][j] = buffer[j];
						if (buffer[j] == 'S')
						{
							Start.x = i;
							Start.y = j;
						}
						if (buffer[j] == 'G')
						{
							Goal.x = j;
							Goal.y = i;
						}
					}
					fscanf(mazeFile, "%s", buffer);
				}
			}
		}
		fscanf(mazeFile, "%s", buffer);
	}
}

Maze::~Maze()
{
}
