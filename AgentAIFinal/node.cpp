#include "node.h"

Node::Node()
{
	linkCount = 0;
	distance = 0;
	visited = false;
}

void Node::AddLink(Node * toLink)
{
	link[linkCount] = toLink;
	linkCount++;
}
