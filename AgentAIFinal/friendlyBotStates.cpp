#include "friendlyBotStates.h"
#include "friendlyBot.h"
#include <iostream>
using namespace std;
//create static idle instance
Idle * Idle::Instance()
{
	static Idle instance;
	return &instance;
}
//when entering idle...
void Idle::Enter(friendlyBot* ptrBot)
{
	//print this
	cout << "I'm Idle Now!\n";
}

void Idle::Execute(friendlyBot* ptrBot)
{
	cout << "I'm idling here!";
	//call readyToGo to check if a state change is needed
	if (ptrBot->readyToGo())
	{
		//if a change is needed, change state to searching
		//true implementation would include a more thorough check against more conditions, to allow the ai to pass into many different states from idle
		ptrBot->ChangeState(Searching::Instance());
	}
}

//on idle exit
void Idle::Exit(friendlyBot* ptrBot)
{
	//print this
	cout << "\nGot stuff to do, can't be idling\n";
}
//create static searching instance
Searching * Searching::Instance()
{
	static Searching instance;
	return &instance;
}
//on enetering searching...
void Searching::Enter(friendlyBot* ptrBot)
{
	//print this
	cout << "Now I'm Searching!\n";
}
//searchiung state execute function - noted that the searching of a passed in maze can be decoupled from here
void Searching::Execute(friendlyBot* ptrBot)
{
	//if no more positions
	if (ptrBot->knownPositions.isEmpty())
	{
		//searching is complete, return to idle
		ptrBot->ChangeState(Idle::Instance());
	}
	//if goal is found
	else if (ptrBot->foundGoal)
	{
		//searching is complete, move to success state
		ptrBot->ChangeState(Success::Instance());
	}
	else
	{
		//pop position from stack, make it last visited
		ptrBot->lastVisited = ptrBot->knownPositions.pop();
		//initialise curNOde, tracks node we are currently at
		int curNode = 0;
		//make the bots actual position that of the popped position - note: investigate wether lastvisted and actual are duplicate, could remove lastvisited and use actual in its place
		ptrBot->actual = ptrBot->lastVisited;
		//for each node in Graph...
		for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
		{
			//if position is equal to position of a node...
			if (ptrBot->actual.x == ptrBot->initialGraph.nodelist[i].pos.x && ptrBot->actual.y == ptrBot->initialGraph.nodelist[i].pos.y)
			{
				//node we are at is that node
				curNode = i;
			}
		}
		//for 4 directions, do the following
		for (int i = 1; i < 5; i++)
		{
			//initialise variables
			int x;
			int y;
			Position topush;
			//depending on the direction, where numbers 1-4 represent direction...
			switch (i)
			{
				//if cases are unmet break
			default:
				break;
				//searching down
			case 1:
			{
				//set searched variables as one tile down from bot
				x = ptrBot->actual.x + 1;
				y = ptrBot->actual.y;
				//depending on tile character
				switch (ptrBot->ptrMaze->tile[x][y])
				{
				default:
					break;
					//if pipe, it's a wall, set corresponding tile position in diuscovered to this, does not need a node
				case '|':
					ptrBot->discovered[x][y] = '|';
					break;
					//if f it's a floor tile
				case 'f':
				{
					//set position to push to stack
					topush.x = x;
					topush.y = y;
					//if bot has already seen this tile from a different tile, node will already exist, need to link to existing node, 
					//position will have either been visited already or in stack waiting to be visited
					if (ptrBot->discovered[x][y] != '#')
					{
						// search all nodes for node position. Should not have two nodes with same position
						for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
						{
							if (ptrBot->initialGraph.nodelist[i].pos.x == topush.x && ptrBot->initialGraph.nodelist[i].pos.y == topush.y)
							{
								if (ptrBot->initialGraph.nodelist[curNode].linkCount < 4)
								{
									ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[i]);
								}

							}
						}
					}
					//if bot has not seen tile
					else
					{
						//set tile as seen as floor by bot
						ptrBot->discovered[x][y] = 'f';
						//push position onto stack
						ptrBot->knownPositions.push(topush);
						//set relevant node information
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].pos = topush;
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].key = 'f';
						//add link to node we are at
						ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount]);
						//move node position to next slot
						ptrBot->initialGraph.nodeCount++;
					}
				}
				break;
				//for case G, see ccase f for identical logic - additional logic is commented on here
				case 'G':
				{
					topush.x = x;
					topush.y = y;
					if (ptrBot->discovered[x][y] != '#')
					{
						for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
						{
							if (ptrBot->initialGraph.nodelist[i].pos.x == topush.x && ptrBot->initialGraph.nodelist[i].pos.y == topush.y)
							{
								if (ptrBot->initialGraph.nodelist[curNode].linkCount < 4)
								{
									ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[i]);
								}
							}
						}
					}
					else
					{
						ptrBot->discovered[x][y] = 'G';
						ptrBot->knownPositions.push(topush);
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].pos = topush;
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].key = 'G';
						ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount]);
						//goal has been seen, add goal info to bot
						ptrBot->foundGoal = true;
						ptrBot->goalpos = topush;
						ptrBot->initialGraph.nodeCount++;
					}
				}
				break;
				//for case G, see ccase f for identical logic - additional logic is commented on here
				case 'S':
				{
					topush.x = x;
					topush.y = y;
					if (ptrBot->discovered[x][y] != '#')
					{
						for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
						{
							if (ptrBot->initialGraph.nodelist[i].pos.x == topush.x && ptrBot->initialGraph.nodelist[i].pos.y == topush.y)
							{
								if (ptrBot->initialGraph.nodelist[curNode].linkCount < 4)
								{
									ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[i]);
								}

							}
						}
					}
					else
					{
						ptrBot->discovered[x][y] = 'S';
						ptrBot->knownPositions.push(topush);
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].pos = topush;
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].key = 'S';
						ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount]);
						ptrBot->initialGraph.nodeCount++;
					}
				}
				break;

				}
			}
			break;
			//searching left
			//for case 2, see case 1 for identical logic. No additional logic appears
			case 2:
			{
				x = ptrBot->actual.x;
				y = ptrBot->actual.y - 1;
				switch (ptrBot->ptrMaze->tile[x][y])
				{
				default:
					break;
				case '|':
					ptrBot->discovered[x][y] = '|';
					break;
				case 'f':
				{
					topush.x = x;
					topush.y = y;
					if (ptrBot->discovered[x][y] != '#')
					{
						for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
						{
							if (ptrBot->initialGraph.nodelist[i].pos.x == topush.x && ptrBot->initialGraph.nodelist[i].pos.y == topush.y)
							{
								if (ptrBot->initialGraph.nodelist[curNode].linkCount < 4)
								{
									ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[i]);
								}
							}
						}
					}
					else
					{
						ptrBot->discovered[x][y] = 'f';
						ptrBot->knownPositions.push(topush);
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].pos = topush;
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].key = 'f';
						ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount]);

						ptrBot->initialGraph.nodeCount++;
					}
				}
				break;
				case 'G':
				{
					topush.x = x;
					topush.y = y;
					if (ptrBot->discovered[x][y] != '#')
					{
						for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
						{
							if (ptrBot->initialGraph.nodelist[i].pos.x == topush.x && ptrBot->initialGraph.nodelist[i].pos.y == topush.y)
							{
								if (ptrBot->initialGraph.nodelist[curNode].linkCount < 4)
								{
									ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[i]);
								}
							}
						}
					}
					else
					{
						ptrBot->discovered[x][y] = 'G';
						ptrBot->knownPositions.push(topush);
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].pos = topush;
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].key = 'G';
						ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount]);
						ptrBot->foundGoal = true;
						ptrBot->goalpos = topush;
						ptrBot->initialGraph.nodeCount++;
					}
				}
				break;

				case 'S':
				{
					topush.x = x;
					topush.y = y;
					if (ptrBot->discovered[x][y] != '#')
					{
						for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
						{
							if (ptrBot->initialGraph.nodelist[i].pos.x == topush.x && ptrBot->initialGraph.nodelist[i].pos.y == topush.y)
							{
								if (ptrBot->initialGraph.nodelist[curNode].linkCount < 4)
								{
									ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[i]);
								}

							}
						}
					}
					else
					{
						ptrBot->discovered[x][y] = 'S';
						ptrBot->knownPositions.push(topush);
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].pos = topush;
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].key = 'S';
						ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount]);
						ptrBot->initialGraph.nodeCount++;
					}
				}
				break;

				}
			}
			break;
			//searching up
			//for case 3, see case 1 for identical logic. No additional logic appears
			case 3:
			{
				x = ptrBot->actual.x - 1;
				y = ptrBot->actual.y;
				switch (ptrBot->ptrMaze->tile[x][y])
				{
				default:
					break;
				case '|':
					ptrBot->discovered[x][y] = '|';
					break;
				case 'f':
				{
					topush.x = x;
					topush.y = y;
					if (ptrBot->discovered[x][y] != '#')
					{
						for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
						{
							if (ptrBot->initialGraph.nodelist[i].pos.x == topush.x && ptrBot->initialGraph.nodelist[i].pos.y == topush.y)
							{
								if (ptrBot->initialGraph.nodelist[curNode].linkCount < 4)
								{
									ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[i]);
								}
							}
						}
					}
					else
					{
						ptrBot->discovered[x][y] = 'f';
						ptrBot->knownPositions.push(topush);
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].pos = topush;
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].key = 'f';
						ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount]);
						ptrBot->initialGraph.nodeCount++;
					}
				}
				break;

				case 'G':
				{
					topush.x = x;
					topush.y = y;
					if (ptrBot->discovered[x][y] != '#')
					{
						for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
						{
							if (ptrBot->initialGraph.nodelist[i].pos.x == topush.x && ptrBot->initialGraph.nodelist[i].pos.y == topush.y)
							{
								if (ptrBot->initialGraph.nodelist[curNode].linkCount < 4)
								{
									ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[i]);
								}
							}
						}
					}
					else
					{
						ptrBot->discovered[x][y] = 'G';
						ptrBot->knownPositions.push(topush);
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].pos = topush;
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].key = 'G';
						ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount]);
						ptrBot->initialGraph.nodeCount++;
						ptrBot->foundGoal = true;
						ptrBot->goalpos = topush;
					}
				}
				break;

				case 'S':
				{
					topush.x = x;
					topush.y = y;
					if (ptrBot->discovered[x][y] != '#')
					{
						for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
						{
							if (ptrBot->initialGraph.nodelist[i].pos.x == topush.x && ptrBot->initialGraph.nodelist[i].pos.y == topush.y)
							{
								if (ptrBot->initialGraph.nodelist[curNode].linkCount < 4)
								{
									ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[i]);
								}

							}
						}
					}
					else
					{
						ptrBot->discovered[x][y] = 'S';
						ptrBot->knownPositions.push(topush);
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].pos = topush;
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].key = 'S';
						ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount]);
						ptrBot->initialGraph.nodeCount++;
					}
				}
				break;
				}
			}
			break;
			//searching right
			//for case 3, see case 1 for identical logic. No additional logic appears
			case 4:
			{
				x = ptrBot->actual.x;
				y = ptrBot->actual.y + 1;
				switch (ptrBot->ptrMaze->tile[x][y])
				{
				default:
					break;
				case '|':
					ptrBot->discovered[x][y] = '|';
					break;
				case 'f':
				{
					topush.x = x;
					topush.y = y;
					if (ptrBot->discovered[x][y] != '#')
					{
						for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
						{
							if (ptrBot->initialGraph.nodelist[i].pos.x == topush.x && ptrBot->initialGraph.nodelist[i].pos.y == topush.y)
							{
								if (ptrBot->initialGraph.nodelist[curNode].linkCount < 4)
								{
									ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[i]);
								}
							}
						}
					}
					else
					{
						ptrBot->discovered[x][y] = 'f';
						ptrBot->knownPositions.push(topush);
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].pos = topush;
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].key = 'f';
						ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount]);
						ptrBot->initialGraph.nodeCount++;
					}
				}
				break;

				case 'G':
				{
					topush.x = x;
					topush.y = y;
					if (ptrBot->discovered[x][y] != '#')
					{
						for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
						{
							if (ptrBot->initialGraph.nodelist[i].pos.x == topush.x && ptrBot->initialGraph.nodelist[i].pos.y == topush.y)
							{
								if (ptrBot->initialGraph.nodelist[curNode].linkCount < 4)
								{
									ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[i]);
								}
							}
						}
					}
					else
					{
						ptrBot->discovered[x][y] = 'G';
						ptrBot->knownPositions.push(topush);
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].pos = topush;
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].key = 'G';
						ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount]);
						ptrBot->initialGraph.nodeCount++;
						ptrBot->foundGoal = true;
						ptrBot->goalpos = topush;
					}
				}
				break;

				case 'S':
				{
					topush.x = x;
					topush.y = y;
					if (ptrBot->discovered[x][y] != '#')
					{
						for (int i = 0; i < ptrBot->initialGraph.nodeCount; i++)
						{
							if (ptrBot->initialGraph.nodelist[i].pos.x == topush.x && ptrBot->initialGraph.nodelist[i].pos.y == topush.y)
							{
								if (ptrBot->initialGraph.nodelist[curNode].linkCount < 4)
								{
									ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[i]);
								}

							}
						}
					}
					else
					{
						ptrBot->discovered[x][y] = 'S';
						ptrBot->knownPositions.push(topush);
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].pos = topush;
						ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount].key = 'S';
						ptrBot->initialGraph.nodelist[curNode].AddLink(&ptrBot->initialGraph.nodelist[ptrBot->initialGraph.nodeCount]);
						ptrBot->initialGraph.nodeCount++;
					}
				}
				break;

				}
				break;
			}
			}
		}
		//prints bots known positions in a readable format
		cout << "\n\n\n";
		for (int a = 0; a < ptrBot->ptrMaze->height + 3; a++)
		{
			for (int b = 0; b < ptrBot->ptrMaze->width + 10; b++)
			{
				if (ptrBot->actual.x == a && ptrBot->actual.y == b)
				{
					cout << "@";
				}
				else
				{
					cout << ptrBot->discovered[a][b];
				}

			}
			cout << "\n";
		}
		cout << "\n\n\n";

	}
}
//on exit...
void Searching::Exit(friendlyBot* ptrBot)
{
	//print this
	cout << "\nI'm done searching boss!\n";
}
//create static success instance
Success * Success::Instance()
{
	static Success instance;
	return &instance;
}
//on enter...
void Success::Enter(friendlyBot * ptrBot)
{
	//print this
	cout << "I've found it boss!\n";
}

void Success::Execute(friendlyBot * ptrBot)
{
	//check if Bot should return to idle or continue searching the whole maze
	if (ptrBot->foundGoalWhatNext())
	{
		//if idle, go idle
		ptrBot->ChangeState(Idle::Instance());
	}
	else
	{
		//if not, switch found goal off and search maze
		//this has the effect of the bot not searching for a goal, but for the rest of the maze
		//if multiple goals exist, bot should stop for second goal still
		ptrBot->foundGoal = false;
		ptrBot->ChangeState(Searching::Instance());
	}
}

void Success::Exit(friendlyBot * ptrBot)
{
}