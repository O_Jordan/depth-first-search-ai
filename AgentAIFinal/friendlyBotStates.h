#pragma once
#include "State.h"
class friendlyBot;

class Idle : public State
{
	//contains states for the ai to use, a static instance, and an enter exit and execute function
public:
	Idle(){};
	static Idle* Instance();
	virtual void Enter(friendlyBot*);
	virtual void Execute(friendlyBot*);
	virtual void Exit(friendlyBot*);
};

class Searching : public State
{
public:
	Searching() {};
	static Searching* Instance();
	virtual void Enter(friendlyBot*);
	virtual void Execute(friendlyBot*);
	virtual void Exit(friendlyBot*);
};

class Success : public State
{
public:
	Success() {};
	static Success* Instance();
	virtual void Enter(friendlyBot*);
	virtual void Execute(friendlyBot*);
	virtual void Exit(friendlyBot*);
};