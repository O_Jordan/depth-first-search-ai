#include "friendlyBot.h"
#include "friendlyBotStates.h"
#include <iostream>
using namespace std;
friendlyBot::friendlyBot(Position start, Maze* ptr)
{
	//initialise discovered as blank spaces
	for (int i = 0; i < 50; i++)
	{
		for (int j = 0; j < 50; j++)
		{
			discovered[i][j] = '#';
		}
	}
	//initialise variables
	ptrMaze = ptr;
	ptrCurState = Idle::Instance();
	actual = start;
	lastVisited = start;
	//push starting position into stack of known positions
	knownPositions.push(actual);
	initialGraph.nodelist[initialGraph.nodeCount].pos = actual;
	initialGraph.nodelist[initialGraph.nodeCount].key = 'f';
	initialGraph.nodeCount++;
	discovered[actual.x][actual.y] = 'S';
	foundGoal = false;
	route[0] = actual;
	routeCount = 1;
	goalpos.x = 0;
	goalpos.y = 0;
}

friendlyBot::~friendlyBot()
{
}

void friendlyBot::Update()
{
	//execute current states logic
	ptrCurState->Execute(this);
}

void friendlyBot::ChangeState(State * ptrNextState)
{
	//transition into new state
	ptrCurState->Exit(this);
	ptrCurState = ptrNextState;
	ptrCurState->Enter(this);
}

bool friendlyBot::readyToGo()
{
	//algorithm asks user if we're ready - will return result to idle state to check if a state change is needed
	//ideally would be a check to see if the AI needs to do something, i.e. there's an enemy in sight, or we have to find something ect.
	int reply;
	cout << "\nReady to go? We're still idle here! (Type 1 for yes, 2 for no)\n";
	cin >> reply;
	if (reply == 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool friendlyBot::foundGoalWhatNext()
{
	//requests action after finding a goal, in similar fashion to readyToGo()
	int reply;
	cout << "\nWe have the goal, want to explore the rest of the maze? (1 for return to idle, 2 for continue search)";
	cin >> reply;
	if (reply == 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}
