#pragma once
class State;
#include "Stack.h"
#include "Position.h"
#include "Maze.h"
#include "graph.h"
class friendlyBot
{
private:
	State* ptrCurState;
	
public:
	friendlyBot(Position start, Maze* ptr);
	Position actual;
	Maze* ptrMaze;
	~friendlyBot();
	void Update();
	void ChangeState(State* ptrNextState);
	bool readyToGo();
	bool foundGoalWhatNext();
	Stack knownPositions;
	Position lastVisited;
	char discovered[50][50];
	Graph initialGraph;

	Position route[100];
	int routeCount;
	Position goalpos;
	bool foundGoal;
};