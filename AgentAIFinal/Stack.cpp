#include "Stack.h"
#include <iostream>
using namespace std;
Stack::Stack()
{
	stackCount = -1;
	pstnList = new Position[STACK_SIZE];
}

Stack::~Stack()
{
}

void Stack::push(Position x)
{
	if (!isFull())
	{
		stackCount++;
		pstnList[stackCount] = x;
	}
	else
	{
		cout << "Stack is full!"; //need error catch here, what to do when stack is full? start new stack? Stack size is set in stone, so could allow recursive creation of a stack...
		//consider how AI will react when stack is full, perhaps First in First Out, only remember back a certain way
	}
}

Position Stack::pop()
{
	if (!isEmpty())
	{
		Position temp = pstnList[stackCount];
		stackCount--;
		return temp;
	}
	else
	{
		cout << "Stack is empty!"; //same as push, work out how AI will react
		Position blank;
		blank.x = 0;
		blank.y = 0;
		return blank;
	}
}

bool Stack::isFull()
{
	if (stackCount >= STACK_SIZE - 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Stack::isEmpty()
{
	if (stackCount < 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
