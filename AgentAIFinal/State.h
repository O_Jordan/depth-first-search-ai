#pragma once

class friendlyBot;

class State
{
public:
	virtual void Enter(friendlyBot*) = 0;
	virtual void Execute(friendlyBot*) = 0;
	virtual void Exit(friendlyBot*) = 0;
};